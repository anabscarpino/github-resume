﻿namespace GitHubResume.Entities
{
    public class FileResume
    {
        public string extension { get; set; }
        public int count { get; set; }
        public int lines { get; set; }
        public long bytes { get; set; }

        public FileResume() { }
    }
}
