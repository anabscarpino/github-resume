﻿namespace GitHubResume.Models.Responses
{
    public class FileResumeResponse
    {
        public string extension { get; set; }
        public int count { get; set; }
        public int lines { get; set; }
        public double bytes { get; set; }
    }
}
