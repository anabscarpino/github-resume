﻿using AutoMapper;
using GitHubResume.Entities;
using GitHubResume.Models.Responses;
using GitHubResume.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace GitHubResume.Controllers
{
    [Route("api/fileresume")]
    [ApiController]
    public class FileResumeController : ControllerBase
    {
        IMapper _mapper;
        private readonly IFileResumeService fileResumeService;

        public FileResumeController(IFileResumeService fileResumeService, IMapper mapper)
        {
            this.fileResumeService = fileResumeService;
            this._mapper = mapper;
        }

        [HttpGet("{username}/{repository}")]
        public ActionResult<IEnumerable<FileResumeResponse>> Get(string username, string repository)
        {
            IEnumerable<FileResumeResponse> response = _mapper.Map<IEnumerable<FileResumeResponse>>(this.fileResumeService.GetResume(username, repository, null));
            return Ok(response);
        }
    }
}
