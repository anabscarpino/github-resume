﻿using GitHubResume.Entities;
using GitHubResume.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace GitHubResume.Services
{
    public class FileResumeService : IFileResumeService
    {
        public List<FileResume> files = new List<FileResume>();
        private int foldersCount = 0;

        public FileResumeService(){ }

        public List<FileResume> GetResume(string username, string repository, string folder)
        {
            WebClient github = new WebClient();

            string data = GetWebData(username, repository, folder, github);

            Regex regex = new Regex(".*?href=\"(.*?)\">(.*?)</a>");
            MatchCollection html = regex.Matches(data);
            

            foreach (var tagA in html)
            {
                string href = tagA.ToString();
                if (href.Contains("/master/"))
                {
                    string[] hrefParts = href.Split("/master/");
                    string nameFile = "";
                    string[] nameInParts = hrefParts[1].Substring(0, hrefParts[1].IndexOf('"')).Split('.');
                    for(int i=0; i < nameInParts.Length; i++)
                    {
                        if(i == nameInParts.Length - 1){ nameFile = nameFile + nameInParts[i]; }
                        else { nameFile = nameFile + nameInParts[i] + "."; }
                    }

                    bool isFolder = hrefParts[0].Contains("/tree");

                    if (isFolder)
                    {
                        foldersCount = foldersCount + 1;
                        int firstIndex = hrefParts[1].IndexOf('>');
                        int lastIndex = hrefParts[1].IndexOf('<');
                        var nameFolder = nameInParts[0];
                        this.GetResume(username, repository, nameFolder);
                    }
                    else
                    {
                        string extension;

                        if (nameInParts.Length != 1) { extension = nameInParts[nameInParts.Length - 1]; }
                        else { extension = ""; }

                        string content = GetBytesLinesInfo(username, repository, folder, github, nameFile);

                        String[] bytesDelimiters = { " Bytes", " KB", " MB", " GB" };
                        var fileBytesInfo = content.Split(bytesDelimiters, StringSplitOptions.None);
                        int factor = GetBytesFactor(content);
                        double bytes;
                        if (factor != 0) { bytes = Convert.ToDouble(fileBytesInfo[0].Substring(fileBytesInfo[0].LastIndexOf(' ')).Replace('.', ',')) * factor; }
                        else { bytes = 0; }

                        var fileLinesInfo = content.Split(" lines");
                        int lines;
                        if (content.Contains(" lines")) { lines = Convert.ToInt32(fileLinesInfo[0].Substring(fileLinesInfo[0].LastIndexOf(' '))); }
                        else { lines = 0; }

                        CreateOrUpdateFilesList(extension, bytes, lines);
                    }
                }
            }
            return files;
        }

        private static int GetBytesFactor(string content)
        {
            int factor = 0;

            if (content.Contains(" KB")) { factor = 1000; }
            else if (content.Contains(" MB")) { factor = 1000000; }
            else if (content.Contains(" GB")) { factor = 1000000000; }
            else if (content.Contains(" Bytes")) { factor = 1; }

            return factor;
        }

        private void CreateOrUpdateFilesList(string extension, double bytes, int lines)
        {
            FileResume file = files.FirstOrDefault(x => x.extension.Equals(extension));

            if (file != null)
            {
                file.count = file.count + 1;
                file.bytes = file.bytes + (long)bytes;
                file.lines = file.lines + lines;
            }
            else
            {
                file = new FileResume();
                file.extension = extension;
                file.count = 1;
                file.bytes = (long)bytes;
                file.lines = lines;
                files.Add(file);
            }
        }

        private static string GetBytesLinesInfo(string username, string repository, string folder, WebClient github, string nameFile)
        {
            string content;
            if (!String.IsNullOrEmpty(folder)) { content = github.DownloadString("https://github.com/" + username + "/" + repository + "/blob/master/" + nameFile +"?_pjax=%23repo-content-pjax-container"); }
            else { content = github.DownloadString("https://github.com/" + username + "/" + repository + "/blob/master/" + nameFile + "?_pjax=%23repo-content-pjax-container"); }

            return content;
        }

        private static string GetWebData(string username, string repository, string folder, WebClient web1)
        {
            string data;
            if (String.IsNullOrEmpty(folder)) { data = web1.DownloadString("https://github.com/" + username + "/" + repository + "/file-list/master"); }
            else { data = web1.DownloadString("https://github.com/" + username + "/" + repository + "/file-list/master/" + folder); }

            return data;
        }
    }
}
