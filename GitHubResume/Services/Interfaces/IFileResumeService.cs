﻿using GitHubResume.Entities;
using System.Collections.Generic;

namespace GitHubResume.Services.Interfaces
{
    public interface IFileResumeService
    {
        List<FileResume> GetResume(string username, string repository, string folder);

    }
}
