﻿using AutoMapper;
using GitHubResume.Entities;
using GitHubResume.Models.Responses;
using GitHubResume.Services;
using GitHubResume.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace GitHubResume
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddScoped<IFileResumeService, FileResumeService>();

            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<FileResume, FileResumeResponse> ();
            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            services.AddLogging(
                            builder =>
                            {
                                builder.AddFilter("Microsoft", LogLevel.None)
                                       .AddFilter("System", LogLevel.None)
                                       .AddFilter("NToastNotify", LogLevel.None)
                                       .AddConsole();
                            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
               // app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            

            app.UseHttpsRedirection();
            app.UseMvc();

            app.Run(context =>
            {
                return context.Response.WriteAsync("Hello from ASP.NET Core!");
            });
        }
    }
}
